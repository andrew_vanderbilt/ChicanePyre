# ChicanePyre

chicane - to trick or deceive, or a serpentine bend in a road.

## Before you start
ChicanePyre is a toy proof of concept dropper and persistence agent made for the study of Automator-based malware. You may not run any of the components on unauthorized devices. You may not weaponize the existing code for illegal purposes. Vander Security is not responsible for damages arising from the use of this software.

## Using ChicanePyre
ChicanePyre is built from two Automator workflows. Both are exported into standalone apps. The first app is a persistence agent, and will be embedded in the dropper using a base64 encodeded string. The dropper also copies a real Discord app into the Applications folder, and uses several basic persistence techniques to hide in the target system. The default payload is a meterpreter bind TCP on port 4443. The presence of the listener can be a litmus test for whether you have achieved removal of all the persistent code. Given the network component of this malware it is reccomended that should you choose to execute it on a Mac, isolate it from the broader network.

## The Kill Chain
1. Execution of "Discord.app"
2. Dropper establishes persistence and entrenches the agent in various places.
3. Persistence agent opens a Meterpreter bind TCP port on 4443.


## Building ChicanePyre

Building ChicanePyre is a manual process, as our base is Automator. You could probably automate it.

1. Export the persistence_agent.wflow in Automator as an app.
2. Rename the exported app as 'DiscordHelper.app'
3. Copy the icon from Discord using Finder (resource fork abuse).
4. Create a directory called 'DiscordHelper'.
5. Place the 'DiscordHelper' app in the folder you created.
6. Use Disk Utility to create a DMG archive from the DiscordHelper directory.
7. Encode the DMG as base64 file.
8. Paste the base64 encoded DMG file into the dropper.py into the multi-line string encoded_dmg in the function extract_helper_dmg().
9. Encode the real Discord as a base64 file.
10. Paste the base64 encoded Discord DMG file into the dropper.py into the multi-line string encoded_dmg in the function extract_discord_dmg().
11. Encode dropper.py into a base64 file.
12. Use the ChicanePyre.wflow to export an app called Discord.app
13. Copy the icon from Discord using Finder.
14. Paste the dropper.txt file into the Contents directory in the exported app.
15. Create a directory called Discord and place the exported 'Discord.app' in it.
16. Use Disk Utility to create a DMG from that directory.
17. Distribute to hosts.

## FAQ

1. Can I suggest improvements this code to extend functionality?

    A: If you can prove it has an educational purpose, I will accept merge requests implementing new features.

2. It doesn't work on my Mac!

    A: The Xcode Command Line Tools need installed. This is intentional. You must have a functioning python3 interpreter. Depending on your Mac's security settings, you may be blocked from opening unsigned apps.

3. Why not use Ruby?

    A: I don't want this to be able to run on the majority of Macs out of the box. It's not meant to be practical or have a probable use case outside of education.
